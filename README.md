My Bash Profile
===============

This is my shell profile. Most of the scripts for this were shamelessly stolen from [matschaffer](https://github.com/matschaffer/profile).

Installation
------------

    curl -s https://gitlab.com/javallone/profile/raw/master/install | bash
